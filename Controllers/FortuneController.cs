using Microsoft.AspNetCore.Mvc;
using fortune_dotnet.Models;
using fortune_dotnet.Services;
using System.Net.Mime;
using fortune_dotnet.Attributes;
using System.Runtime.InteropServices;

namespace fortune_dotnet.Controllers;

[ApiController]
[Route("/")]
public class FortuneController : Controller
{
    private readonly ILogger<FortuneController> _logger;
    private readonly IFortuneService _fortuneService;

    public FortuneController(ILogger<FortuneController> logger, IFortuneService fortuneService)
    {
        _logger = logger;
        _fortuneService = fortuneService;
    }

    private Fortune MakeFortune(string name)
    {
        var result = _fortuneService.GetFortuneMessage();
        var random = _fortuneService.GetFortuneNumber();
        var hostname = _fortuneService.GetHostName();
        return new Fortune
        {
            Name = name,
            Message = result,
            Number = random,
            Hostname = hostname,
            Version = "1.0-csharp"
        };
    }

    [HttpGet]
    [Route("/")]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml)]
    public Fortune GetFortune([Optional] string name)
    {
        return MakeFortune(name);
    }

    [HttpGet]
    [AcceptsHeader(MediaTypeNames.Text.Plain)]
    [Produces(MediaTypeNames.Text.Plain)]
    [ApiExplorerSettings(IgnoreApi = true)]
    public IActionResult GetFortuneText()
    {
        string name = Request.Query["name"];
        var fortune = MakeFortune(name);
        return Content(fortune.ToString(), "text/plain");
    }

    [HttpGet]
    [AcceptsHeader(MediaTypeNames.Text.Html)]
    [Produces(MediaTypeNames.Text.Html)]
    [ApiExplorerSettings(IgnoreApi = true)]
    public IActionResult GetFortuneHtml()
    {
        string name = Request.Query.ContainsKey("name") ? Request.Query["name"] : string.Empty;
        var fortune = MakeFortune(name);
        return View("index", fortune);
    }

    [HttpPost]
    [AcceptsHeader(MediaTypeNames.Text.Html)]
    [Produces(MediaTypeNames.Text.Html)]
    [ApiExplorerSettings(IgnoreApi = true)]
    public IActionResult GetFortuneHtmlPost()
    {
        string name = Request.Form.ContainsKey("name") ? Request.Form["name"] : string.Empty;
        var fortune = MakeFortune(name);
        return View("index", fortune);
    }
}
