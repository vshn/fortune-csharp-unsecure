namespace fortune_dotnet.Services;

public interface IFortuneService
{
    string GetFortuneMessage();

    int GetFortuneNumber();

    string GetHostName();
}
