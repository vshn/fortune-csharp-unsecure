using System.Net;
using System.Diagnostics;

namespace fortune_dotnet.Services;

public class FortuneService : IFortuneService
{
    public string GetFortuneMessage()
    {
        var process = new Process()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "/bin/sh",
                Arguments = $"-c fortune",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            }
        };

        process.Start();
        string result = process.StandardOutput.ReadToEnd();
        process.WaitForExit();

        return result;
    }

    public int GetFortuneNumber()
    {
        var random = new Random();
        return random.Next(1000);
    }

    public string GetHostName()
    {
        return Dns.GetHostName();
    }
}
