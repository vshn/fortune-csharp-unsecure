using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Routing;

// Inspired from
// https://www.sharepointeurope.com/versioning-asp-net-core-http-apis-using-accept-header/
namespace fortune_dotnet.Attributes;

public class AcceptsHeaderAttribute : ActionMethodSelectorAttribute
{
    private readonly string _acceptType;
    public AcceptsHeaderAttribute(string acceptType)
    {
        this._acceptType = acceptType;
    }

    public override bool IsValidForRequest(RouteContext routeContext, ActionDescriptor action)
    {
        var acceptHeader = routeContext.HttpContext.Request.Headers.ContainsKey("Accept") ? routeContext.HttpContext.Request.Headers["Accept"].ToString() : null;
        if (acceptHeader == null) return false;

        var acceptedValues = acceptHeader.Split(",");
        foreach (var value in acceptedValues)
        {
            var currentValue = value;
            if (value.Contains("+"))
            {
                currentValue = value.Substring(0, value.IndexOf("+"));
            }
            if (currentValue == _acceptType)
            {
                return true;
            }
        }

        return false;
    }
}
